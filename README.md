# dikdik-treasure-hunter
Construct 2 project source for Dikdik: A-Grazing Treasure Hunter

### Game description ###

Dikdik is seeking buried treasure, but her eyesight isn't so good...maybe you can help her out? Take a leisurely stroll in free mode or try to get a high score in Timed Mode!

### What is this? ###

[Dikdik: A-Grazing Treasure Hunter](http://virtually-competent.itch.io/a-grazing-dikdik) was made in 48 hours for the [Teacart 1k](http://itch.io/jam/teacart-1k) and is compatible with the [SHARECART1000](http://sharecart1000.com/img/SHARECART1000guide.png) save system. Our original plans for the game were pretty ambitious, but between the lack of proper SHARECART1000 support for Construct 2 and the very tight development window, we had to drastically reduce the game's scope and most of the effort was [ultimately put into handling the SHARECART save file](https://github.com/donkeyspaceman/sharecart1000-construct2). There are no current plans to continue development on this title.

### How do I get set up? ###

The source for Dikdik: A-Grazing Treasure Hunter was created in [Scirra Construct 2](https://www.scirra.com/construct2), so you'll need that program to edit/run the project. You should be able to open it with the free version, but you'll have limited editing abilities and won't be able to export the game as the event count is well over the limit of 100.

### Who do I talk to? ###

As I already mentioned, Dikdik: A-Grazing Treasure Hunter has been abandoned and is no longer in development. However, if you need to hold someone accountable for this mess, you can contact [@donkeyspaceman on Twitter](https://twitter.com/donkeyspaceman).
